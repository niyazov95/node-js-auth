import {IChanges} from './changes.interface';
import {EUserActions} from '../enums/user.actions.enum';

export interface IUserRef {
  user: string;
  time?: Date;
  values?: IChanges;
  action?: EUserActions;
}
