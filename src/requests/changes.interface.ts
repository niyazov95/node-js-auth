export interface IChanges {
    languageId: string;
    to: string;
    from: string;
}
