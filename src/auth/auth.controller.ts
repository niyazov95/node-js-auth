import { Body, Controller, Get, HttpException, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { RegisterUserDto } from './dtos/register-user.dto';
import { LoginDto } from './dtos/login.dto';
import { IResponse } from '../responses/response.interface';
import { IResponseStatusCode } from '../responses/response-status-codes.enum';
import {IAuthRequest} from '../requests/auth-request.interface';

@Controller('auth')
@ApiUseTags('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {
  }

  @Post('login')
  async login(@Body() loginDto: LoginDto): Promise<IResponse> {
    try {
      return this.authService.login(loginDto);
    } catch (e) {
      throw new HttpException(IResponseStatusCode.InvalidCredentials.toString(), 401);
    }
  }

  @Post('register')
  async register(@Body() registerUserDto: RegisterUserDto) {
    return this.authService.register(registerUserDto);
  }
  @Get('validate')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  validateUser(@Req() request: IAuthRequest) {
    return request.user;
  }
  @Get('data')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  findAll(@Req() request: IAuthRequest) {
    return request.user;
  }
}
