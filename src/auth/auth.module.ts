import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { UserModule } from '../user/user.module';
import { passportModule } from './passport.const';
import { AuthController } from './auth.controller';
import { AuthHelper } from './helpers/authHelper';

@Module({
  imports: [
    passportModule,
    JwtModule.register({
      privateKey: 'L#tm#!ND!',
      publicKey: 'L#tm#!ND!',
      signOptions: {
        expiresIn: '1d',
        algorithm: 'HS384',
      },
      verifyOptions: {
        algorithms: ['HS384'],
      },
    }),
    UserModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, AuthHelper],
  exports: [PassportModule, AuthService],
})
export class AuthModule {
}
