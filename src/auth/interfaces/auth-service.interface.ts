import { RegisterUserDto } from '../dtos/register-user.dto';
import { LoginDto } from '../dtos/login.dto';
import { IResponse } from '../../responses/response.interface';

export interface IAuthService {
  login(loginDto: LoginDto): Promise<IResponse>;

  register(registerUserDto: RegisterUserDto): Promise<IResponse>;
}
