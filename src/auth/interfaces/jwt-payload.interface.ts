/**
 * Created by npm3r on 5/25/2019
 */
import { IUser } from '../../user/interfaces/user.interface';

export interface IJwtPayload {
  user: IUserPayload;
  iad: number;
  exp: number;
}

export type IUserPayload = Pick<IUser, '_id' | 'firstName' | 'lastName' | 'userName'>;
