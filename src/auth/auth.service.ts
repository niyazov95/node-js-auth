import {HttpException, Injectable} from '@nestjs/common';
import {JwtService} from '@nestjs/jwt';
import {Model} from 'mongoose';
import {IJwtPayload} from './interfaces/jwt-payload.interface';
import {InjectModel} from '@nestjs/mongoose';
import {IUser} from '../user/interfaces/user.interface';
import {LoginDto} from './dtos/login.dto';
import {RegisterUserDto} from './dtos/register-user.dto';
import {AuthHelper} from './helpers/authHelper';
import {IAuthService} from './interfaces/auth-service.interface';
import {IResponse} from '../responses/response.interface';
import {IResponseStatusCode} from '../responses/response-status-codes.enum';

@Injectable()
export class AuthService implements IAuthService {
  constructor(
    private readonly jwtService: JwtService,
    @InjectModel('User') private readonly userModel: Model<IUser>,
    private readonly authHelper: AuthHelper,
  ) {}

  async login(loginDto: LoginDto): Promise<IResponse> {
    const user = await this.findOne(loginDto.userName);
    if (user) {
      const userMinimized = {
        _id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        userName: user.userName,
      };
      if (await this.authHelper.compareKey(loginDto.password, user.password)) {
        return {
          statusCode: 200,
          data: {
            user: userMinimized,
            token: await this.authHelper.buildToken((userMinimized as IUser), this.jwtService),
          },
        };
      } else {
        throw new HttpException(IResponseStatusCode.InvalidCredentials.toString(), 401);
      }
    } else {
      throw new HttpException(IResponseStatusCode.InvalidCredentials.toString(), 401);
    }
  }

  async register(registerUserDto: RegisterUserDto): Promise<IResponse> {
    try {
      registerUserDto.password = await this.authHelper.hashKey(registerUserDto.password);
      const createdUser = new this.userModel(registerUserDto);
      await createdUser.save();
      const userMinimized = {
        _id: createdUser._id,
        firstName: createdUser.firstName,
        lastName: createdUser.lastName,
        userName: createdUser.userName,
      };
      return {
        statusCode: 200,
        data: {
          user: userMinimized,
          token: await this.authHelper.buildToken((userMinimized as IUser), this.jwtService),
        },
      };
    } catch (e) {
      throw new HttpException(
        {
          statusCode: e.code,
          data: e.errmsg,
        },
        500,
      );
    }
  }

  async findOne(userName?: string, id?: string): Promise<IUser> {
    return await this.userModel.findOne({
      $or: [{_id: id}, {userName}],
    });
  }

  async validateUser(payload: IJwtPayload): Promise<IUser> {
    try {
      return await this.findOne(null, payload.user._id);
    } catch (e) {
      throw null;
    }
  }
}
