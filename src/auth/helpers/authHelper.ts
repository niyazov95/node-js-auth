import * as bcrypt from 'bcrypt';
import { IUser } from '../../user/interfaces/user.interface';
import { JwtService } from '@nestjs/jwt';

export class AuthHelper {
  saltRounds: number = 10;

  hashKey(key: string, saltRound: number = this.saltRounds) {
    return bcrypt.hash(key, saltRound);
  }

  compareKey(key: string, valueToCompare: string) {
    return bcrypt.compare(key, valueToCompare);
  }

  async buildToken(user: IUser, jwtService: JwtService): Promise<string> {
    return await jwtService.signAsync({ user });
  }
}
