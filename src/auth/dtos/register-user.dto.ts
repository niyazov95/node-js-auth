import { ApiModelProperty } from '@nestjs/swagger';
import { LoginDto } from './login.dto';

export class RegisterUserDto extends LoginDto {
  @ApiModelProperty()
  readonly firstName: string;

  @ApiModelProperty()
  readonly lastName: string;

  @ApiModelProperty()
  readonly confirmPassword: string;
}
