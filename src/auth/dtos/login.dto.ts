import { ApiModelProperty } from '@nestjs/swagger';

export class LoginDto {
  @ApiModelProperty()
  readonly userName: string;

  @ApiModelProperty()
  password: string;
}
