/**
 * Created by npm3r on 5/25/2019
 */
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IUser } from './interfaces/user.interface';
import {IText} from '../text/interfaces/text.interface';

@Injectable()
export class UserService {
  constructor(
      @InjectModel('User') private readonly userModel: Model<IUser>,
      @InjectModel('Text') private readonly textModel: Model<IText>,
  ) {
  }

  async findAll(): Promise<IUser[]> {
    return await this.userModel.find().exec();
  }
  async findCreated(user: IUser): Promise<IText[]> {
    return this.textModel.find(
        {
          'createdBy.user': user._id,
        },
    );
  }
  async findUpdated(user: IUser): Promise<IText[]> {
    return this.textModel.find(
        {
          updatedBy: {
            $elemMatch: {
              user: user._id,
            },
          },
        },
    );
  }
}
