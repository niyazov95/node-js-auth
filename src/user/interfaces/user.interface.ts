import * as mongoose from 'mongoose';

export interface IUser extends mongoose.Document {
  _id: string;
  firstName: string;
  lastName: string;
  userName: string;
  password?: string;
}
