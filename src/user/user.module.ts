import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './schemas/user.schema';
import { passportModule } from '../auth/passport.const';
import {TextSchema} from '../text/schemas/text.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
        { name: 'User', schema: UserSchema },
        { name: 'Text', schema: TextSchema },
      ]),
    passportModule,
  ],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
