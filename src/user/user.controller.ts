import {Controller, Get, Req, UseGuards} from '@nestjs/common';
import { UserService } from './user.service';
import { IUser } from './interfaces/user.interface';
import {ApiBearerAuth, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {IAuthRequest} from '../requests/auth-request.interface';
import {IText} from '../text/interfaces/text.interface';
@Controller('user')
@ApiUseTags('user')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
export class UserController {
  constructor(
    private userService: UserService,
  ) {
  }
  @Get()
  async findAll(): Promise<IUser[]> {
    return this.userService.findAll();
  }
  @Get('texts/created')
  async findCreated(@Req() request: IAuthRequest): Promise<IText[]> {
    return this.userService.findCreated(request.user);
  }
  @Get('texts/updated')
  async findAllWords(@Req() request: IAuthRequest): Promise<IText[]> {
    return this.userService.findUpdated(request.user);
  }
}
