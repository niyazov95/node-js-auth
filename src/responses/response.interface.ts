import { ResponseT } from './response.type';

export interface IResponse {
  statusCode: number;
  data: ResponseT;
}
