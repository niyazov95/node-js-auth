import { IJwtPayload } from '../auth/interfaces/jwt-payload.interface';
import { IValidationResponse } from '../auth/interfaces/validation.response';
export type ResponseT =
  IJwtPayload | IValidationResponse | string[] | string;
